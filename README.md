openssl req -x509 -sha256 -nodes -subj "/CN=CQT test 123" -days 365 -newkey rsa:2048 -keyout alice.pem -out alice.crt

openssl pkcs8 -in alice.pem -outform DER -out alice.pk8 -topk8 -nocrypt