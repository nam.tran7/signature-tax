package com.koffi;

//import com.itextpdf.io.image.ImageDataFactory;
//import com.itextpdf.kernel.geom.Rectangle;
//import com.itextpdf.kernel.pdf.PdfDocument;
//import com.itextpdf.kernel.pdf.PdfReader;
//import com.itextpdf.kernel.pdf.StampingProperties;
//import com.itextpdf.signatures.BouncyCastleDigest;
//import com.itextpdf.signatures.DigestAlgorithms;
//import com.itextpdf.signatures.IExternalDigest;
//import com.itextpdf.signatures.IExternalSignature;
//import com.itextpdf.signatures.PdfPKCS7;
//import com.itextpdf.signatures.PdfSignatureAppearance;
//import com.itextpdf.signatures.PdfSigner;
//import com.itextpdf.signatures.PrivateKeySignature;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.security.GeneralSecurityException;
//import java.security.KeyStore;
//import java.security.PrivateKey;
//import java.security.Security;
//import java.security.cert.Certificate;
//import java.util.List;
//
//import com.itextpdf.signatures.SignatureUtil;
//import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class SignPdfService {

//    public static final String DEST = "sign";
//
//    public static final String KEYSTORE = "pdf/myfile.p12";
//    public static final String SRC = "hello.pdf";
//
//    public static final char[] PASSWORD = "12345678".toCharArray();
//
//    public static final String[] RESULT_FILES = new String[] {
//            "hello.pdf",
//            "hello_signed2.pdf",
//            "hello_signed3.pdf",
//            "hello_signed4.pdf"
//    };
//
//    public void sign(String src, String dest, Certificate[] chain, PrivateKey pk, String digestAlgorithm,
//                     String provider, PdfSigner.CryptoStandard signatureType, String reason, String location)
//            throws GeneralSecurityException, IOException {
//        PdfReader reader = new PdfReader(Thread.currentThread().getContextClassLoader().getResourceAsStream(src));
//        PdfSigner signer = new PdfSigner(reader, new FileOutputStream(dest), new StampingProperties());
//
//        // Create the signature appearance
//        Rectangle rect = new Rectangle(100, 300, 300, 100);
//        PdfSignatureAppearance appearance = signer.getSignatureAppearance();
//
//        appearance.setSignatureCreator("Java 11");
//        appearance.setReason(reason);
//        appearance.setLocation(location);
//        appearance.setPageRect(rect);
//        appearance.setPageNumber(1);
//        appearance.setImage(ImageDataFactory.create(Thread.currentThread().getContextClassLoader().getResourceAsStream("images/tick.png").readAllBytes()));
//        appearance.setImageScale(0.75f);
//
//
//        signer.setFieldName("sig");
//
//        IExternalSignature pks = new PrivateKeySignature(pk, digestAlgorithm, provider);
//        IExternalDigest digest = new BouncyCastleDigest();
//
//        // Sign the document using the detached mode, CMS or CAdES equivalent.
//        signer.signDetached(digest, pks, chain, null, null, null, 0, signatureType);
//    }
//
//    public void test() throws GeneralSecurityException, IOException {
////        File file = new File(DEST);
////        file.mkdirs();
////
////        BouncyCastleProvider provider = new BouncyCastleProvider();
////        Security.addProvider(provider);
////        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
////        ks.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(KEYSTORE), PASSWORD);
////        String alias = ks.aliases().nextElement();
////        PrivateKey pk = (PrivateKey) ks.getKey(alias, PASSWORD);
////        Certificate[] chain = ks.getCertificateChain(alias);
////
////        sign(SRC, RESULT_FILES[0], chain, pk, DigestAlgorithms.SHA256, provider.getName(),
////                PdfSigner.CryptoStandard.CMS, "Test 1", "Viet Nam");
////
////
////        BouncyCastleProvider provider1 = new BouncyCastleProvider();
////        Security.addProvider(provider1);
//
//        verifySignatures("sign/step4.pdf");
//    }
//
//    public void verifySignatures(String path) throws IOException, GeneralSecurityException {
//        PdfDocument pdfDoc = new PdfDocument(new PdfReader(path));
//
//        ValidateService validateService = new ValidateService();
//        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
//        ks.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(KEYSTORE), PASSWORD);
////        validateService.setKeyStore(ks);
////        validateService.verifySignatures(path);
//
//        pdfDoc.close();
//    }
//
//    public PdfPKCS7 verifySignature(SignatureUtil signUtil, String name) throws GeneralSecurityException {
//        PdfPKCS7 pkcs7 = signUtil.readSignatureData(name);
//
//        System.out.println("Signature covers whole document: " + signUtil.signatureCoversWholeDocument(name));
//        System.out.println("Document revision: " + signUtil.getRevision(name) + " of " + signUtil.getTotalRevisions());
//        System.out.println("Integrity check OK? " + pkcs7.verifySignatureIntegrityAndAuthenticity());
//
//        return pkcs7;
//    }
}
