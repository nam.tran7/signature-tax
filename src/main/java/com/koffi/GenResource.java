package com.koffi;


import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

@Path("/gen")
@ApplicationScoped
public class GenResource {
    private final GenEnveloped genEnveloped;
    private final SignPdfService signPdfService;
    private final MultiSignPdfService multiSignPdfService;

    public GenResource() {
        this.multiSignPdfService = new MultiSignPdfService();
        this.signPdfService = new SignPdfService();
        this.genEnveloped = new GenEnveloped();
    }

    @POST
    public String gen(String xml) throws Exception {
        return genEnveloped.genEnveloped1(xml);
    }

    @GET
    public String gen1() throws Exception {
        multiSignPdfService.test();
        return "success";
    }

    @PUT
    public String gen2() throws Exception {
//        signPdfService.test();
        return "success";
    }
}
