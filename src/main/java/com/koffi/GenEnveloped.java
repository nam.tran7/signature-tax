package com.koffi;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dom.DOMStructure;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignatureProperties;
import javax.xml.crypto.dsig.SignatureProperty;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLObject;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;

public class GenEnveloped {
    public static final String XML_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    private final static String OUT_PATH = "envelopedSignature.xml";
    private final static String KEY = "alice.pk8";
    private final static String CERT = "alice.crt";

    private XMLObject getSigningTime(XMLSignatureFactory fac, String signatureId, Document doc) {
        Element signingTime = doc.createElement("SigningTime");
        signingTime.setTextContent(formatDate(Instant.now(), XML_DATE_FORMAT));
        List<XMLStructure> objs = new ArrayList<XMLStructure>();
        objs.add(new DOMStructure(signingTime));
        SignatureProperty signatureProperty = fac.newSignatureProperty(objs, signatureId, null);
        SignatureProperties signatureProperties = fac.newSignatureProperties(Collections.singletonList(signatureProperty), null);
        String signTimeObjectId = UUID.randomUUID().toString();
        XMLObject obj = fac.newXMLObject(Collections.singletonList(signatureProperties), signTimeObjectId, null, null);
        return obj;
    }

    private String formatDate(Instant date, String format) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format).withZone(ZoneOffset.ofHours(7));
        return dateTimeFormatter.format(date);
    }

    public String genEnveloped1(String xml) throws Exception {
        // Create a DOM XMLSignatureFactory that will be used to generate the
        // enveloped signature
        XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

        // Create a Reference to the enveloped document (in this case we are
        // signing the whole document, so a URI of "" signifies that) and
        // also specify the SHA256 digest algorithm and the ENVELOPED Transform.
        DigestMethod dm = fac.newDigestMethod(DigestMethod.SHA256, null);
        List<Transform> transforms = Collections.singletonList(fac.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null));

        // Create the SignedInfo
        CanonicalizationMethod cm = fac.newCanonicalizationMethod(CanonicalizationMethod.INCLUSIVE_WITH_COMMENTS, (C14NMethodParameterSpec) null);
        SignatureMethod sm = fac.newSignatureMethod(SignatureMethod.RSA_SHA256, null);

        // Read a RSA private key
        InputStream is = getClass().getClassLoader().getResourceAsStream(KEY);
        assert is != null;
        byte[] privateKeyByte = IOUtils.toByteArray(is);
        is.close();
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKeyByte);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        RSAPrivateKey privateKey = (RSAPrivateKey) kf.generatePrivate(keySpec);

        // Read a X.509 certificate
        KeyInfoFactory kif = fac.getKeyInfoFactory();
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        X509Certificate cert = (X509Certificate) cf.generateCertificate(Thread.currentThread().getContextClassLoader().getResourceAsStream(CERT));
        List<Object> x509Content = new ArrayList<>();
        x509Content.add(cert.getSubjectX500Principal().getName());
        x509Content.add(cert);
        X509Data x509Data = fac.getKeyInfoFactory().newX509Data(x509Content);

        // Instantiate the document to be signed
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        xml = xml.replaceAll("<MCCQT/>", String.format("<MCCQT>%s</MCCQT>", genCode()));
        InputSource isTem = new InputSource();
        isTem.setCharacterStream(new StringReader(xml));
        Document doc = dbf.newDocumentBuilder().parse(isTem);

        // Create a KeyInfo and add the X509Data to it
        String signatureId = UUID.randomUUID().toString();
        XMLObject obj = getSigningTime(fac, signatureId, doc);
        KeyInfo ki = kif.newKeyInfo(Collections.singletonList(x509Data));

        // Create a DOMSignContext and specify the RSA PrivateKey and
        // location of the resulting XMLSignature's parent element
        Node node = null;
        Node node1 = null;
        for (int i = 0; i < doc.getDocumentElement().getChildNodes().getLength(); i++) {
            if ("DSCKS".equals(doc.getDocumentElement().getChildNodes().item(i).getNodeName())) {
                node1 = doc.getDocumentElement().getChildNodes().item(i);
                for (int ii = 0; ii < node1.getChildNodes().getLength(); ii++) {
                    if ("CQT".equals(node1.getChildNodes().item(ii).getNodeName())) {
                        node = node1.getChildNodes().item(ii);
                        break;
                    }
                }
                break;
            }
        }
        DOMSignContext dsc = new DOMSignContext(privateKey, node);
        List<Reference> references = Collections.singletonList(
                fac.newReference("#" + obj.getId(), dm, null, null, null));
        SignedInfo si = fac.newSignedInfo(cm, sm, references);

        // Create the XMLSignature (but don't sign it yet)
        XMLSignature signature = fac.newXMLSignature(si, ki, Collections.singletonList(obj), signatureId, null);

        // Marshal, generate (and sign) the enveloped signature
        signature.sign(dsc);

        // output the resulting document
        OutputStream os = new FileOutputStream(OUT_PATH);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer trans = tf.newTransformer();
        trans.transform(new DOMSource(doc), new StreamResult(os));
        os.close();

        try (BufferedReader br = new BufferedReader(new FileReader(OUT_PATH))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            String outXml = sb.toString();
            return outXml.replaceAll("&#13;\n", "");
        }
    }

    private String genCode() {
        int leftLimit = 48;
        int rightLimit = 90;
        int len = 32;
        Random random = new Random();
        return random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i >= 65 && i <= 90) || (i >= 48 && i <= 57))
                .limit(len)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }


    public String test() {
        String path = "/Users/tranquocnam/Desktop/SSDT.xml";
        File file = new File(path);
        String encoding = "UTF-8";
        String xml;

        try {
            xml = readXMLFile(path, "<HDon>");
            return xml;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static final List<Charset> array = List.of(
            StandardCharsets.UTF_8,
            StandardCharsets.UTF_16LE,
            StandardCharsets.UTF_16LE,
            StandardCharsets.UTF_16);

    public static String readXMLFile(String path, String strCheck) throws IOException {
        try(var fis = new FileInputStream(path)) {

            byte[] bytes = new byte[fis.available()];
            fis.read(bytes);
            for (Charset charset : array) {
                String xml = new String(bytes, charset);
                if (check(xml, strCheck)) {
                    return xml;
                }
            }

            throw new RuntimeException("Cannot read file");
        }
    }

    private static boolean check(String xml, String strCheck) {
        return xml.contains(strCheck);
    }
}
