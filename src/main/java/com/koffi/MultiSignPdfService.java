package com.koffi;

import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceDictionary;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceStream;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.ExternalSigningSupport;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.PDSignature;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.SignatureInterface;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.SignatureOptions;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.visible.PDVisibleSigProperties;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.visible.PDVisibleSignDesigner;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDSignatureField;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationVerifier;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.util.Store;
import org.graalvm.collections.Pair;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;

public class MultiSignPdfService implements SignatureInterface {
    PrivateKey privateKey;
    Certificate[] certChain;
    Certificate certificate;

    public static final String DEST = "sign/";
    public static final String FORM = "alo.pdf";
    public static final String NAM = "pdf/myfile.p12";
    public static final String NHAN = "pdf/myfile1.p12";
    public static final String TUAN = "pdf/myfile2.p12";
    public static final String KHOI = "pdf/myfile3.p12";

    public static final char[] PASSWORD = "12345678".toCharArray();

    public static final String[] RESULT_FILES = new String[] {
            "step1.pdf",
            "step2.pdf",
            "step3.pdf",
            "step4.pdf",
            "step5.pdf"
    };

    public static final List<Pair<Float, Float>> location = List.of(
            Pair.create(72F, 710F),
            Pair.create(72F, 610F),
            Pair.create(72F, 520F),
            Pair.create(72F, 420F)
    );

    public void test() throws IOException, GeneralSecurityException {
        File file = new File(DEST);
        file.mkdirs();

        BouncyCastleProvider provider = new BouncyCastleProvider();
        Security.addProvider(provider);

        sign(NAM, provider.getName(), FORM, "sig1", DEST + RESULT_FILES[0], location.get(0));
//        sign(NHAN, provider.getName(), DEST + RESULT_FILES[0], "sig2", DEST + RESULT_FILES[1], location.get(1));
//        sign(TUAN, provider.getName(), DEST + RESULT_FILES[1], "sig3", DEST + RESULT_FILES[2], location.get(2));
//        sign(KHOI, provider.getName(), DEST + RESULT_FILES[2], "sig4", DEST + RESULT_FILES[3], location.get(3));


        verify(DEST + RESULT_FILES[0]);
    }

    public void sign(String ks, String provider, String src, String name, String dest, Pair<Float, Float> position)
            throws GeneralSecurityException, IOException {
        KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
        keystore.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(ks), PASSWORD);

        PrivateKey oPrivateKey = null;
        for (Enumeration<String> oEnum = keystore.aliases(); oEnum.hasMoreElements(); ) {
            String sAlias = (String) oEnum.nextElement();
            oPrivateKey = (PrivateKey) keystore.getKey(sAlias, PASSWORD);
            Certificate[] certChain = keystore.getCertificateChain(sAlias);
            if (certChain == null) {
                continue;
            }
            if (oPrivateKey == null) {
                continue;
            }

            certificate = keystore.getCertificate(sAlias);
            privateKey = oPrivateKey;
            this.certChain = certChain;

            signDetached(src, dest, name);

            break;
        }
    }

    public void signDetached(String src, String dest, String name) throws IOException {

        FileOutputStream fos = new FileOutputStream(dest);

        PDDocument doc = PDDocument.load(new File(src));
        signDetached(doc, fos, name);
        doc.close();
        fos.close();
    }

    public void signDetached(PDDocument document, OutputStream output, String name) throws IOException {

        int accessPermissions = getMDPPermission(document);
        System.out.println(accessPermissions);
        if (accessPermissions == 1) {
            throw new IllegalStateException("No changes to the document are permitted due to DocMDP transform parameters dictionary");
        }

        // create signature dictionary
        PDSignature signature = new PDSignature();

        signature.setFilter(PDSignature.FILTER_ADOBE_PPKLITE);
        signature.setSubFilter(PDSignature.SUBFILTER_ADBE_PKCS7_DETACHED);
        signature.setName(name);
        signature.setLocation("Viet Nam");
        signature.setReason("Test");
        signature.setSignDate(Calendar.getInstance());

        PDVisibleSignDesigner visibleSig = new PDVisibleSignDesigner(document, Thread.currentThread().getContextClassLoader().getResourceAsStream("images/tick.png"), 1);
        visibleSig
                .xAxis(200)
                .yAxis(300)
                .zoom(-50)
                .signatureFieldName("signature");
        PDVisibleSigProperties signatureProperties = new PDVisibleSigProperties();
        signatureProperties
                .signerName(name)
                .signerLocation("Viet Nam")
                .signatureReason("Test")
                .preferredSize(0)
                .page(1)
                .visualSignEnabled(true)
                .setPdVisibleSignature(visibleSig)
                .buildSignature();

        //  certify
        if (accessPermissions == 0) {
            setMDPPermission(document, signature, 2);
        }

        SignatureOptions options = new SignatureOptions();
        options.setVisualSignature(signatureProperties);
        // options.setPage(signatureProperties.getPage());
        // options.setPreferedSignatureSize(signatureProperties.getPreferredSize());
        document.addSignature(signature, this, options);


        ExternalSigningSupport externalSigning = document.saveIncrementalForExternalSigning(output);
        // invoke external signature service
        byte[] cmsSignature = sign(externalSigning.getContent());
        // set signature bytes received from the service
        externalSigning.setSignature(cmsSignature);
    }

    @Override
    public byte[] sign(InputStream content) throws IOException {
        try {
            List<Certificate> certList = new ArrayList<Certificate>(Arrays.asList(certChain));
            certList.add(certificate);
            Store certs = new JcaCertStore(certList);
            CMSSignedDataGenerator gen = new CMSSignedDataGenerator();
            org.bouncycastle.asn1.x509.Certificate cert = org.bouncycastle.asn1.x509.Certificate.getInstance(ASN1Primitive.fromByteArray(certificate.getEncoded()));
            ContentSigner sha1Signer = new JcaContentSignerBuilder("SHA1WithRSA").build(privateKey);
            gen.addSignerInfoGenerator(new JcaSignerInfoGeneratorBuilder(new JcaDigestCalculatorProviderBuilder().build()).build(sha1Signer, new X509CertificateHolder(cert)));
            gen.addCertificates(certs);
            CMSProcessableInputStream msg = new CMSProcessableInputStream(content);
            CMSSignedData signedData = gen.generate(msg, false);

            return signedData.getEncoded();
        } catch (GeneralSecurityException | CMSException | OperatorCreationException e) {
            throw new IOException(e);
        }
    }

    public int getMDPPermission(PDDocument doc) {
        COSBase base = doc.getDocumentCatalog().getCOSObject().getDictionaryObject(COSName.PERMS);
        if (base instanceof COSDictionary) {
            COSDictionary permsDict = (COSDictionary) base;
            base = permsDict.getDictionaryObject(COSName.DOCMDP);
            if (base instanceof COSDictionary) {
                COSDictionary signatureDict = (COSDictionary) base;
                base = signatureDict.getDictionaryObject("Reference");
                if (base instanceof COSArray) {
                    COSArray refArray = (COSArray) base;
                    for (int i = 0; i < refArray.size(); ++i) {
                        base = refArray.getObject(i);
                        if (base instanceof COSDictionary) {
                            COSDictionary sigRefDict = (COSDictionary) base;
                            if (COSName.DOCMDP.equals(sigRefDict.getDictionaryObject("TransformMethod"))) {
                                base = sigRefDict.getDictionaryObject("TransformParams");
                                if (base instanceof COSDictionary) {
                                    COSDictionary transformDict = (COSDictionary) base;
                                    int accessPermissions = transformDict.getInt(COSName.P, 2);
                                    if (accessPermissions < 1 || accessPermissions > 3) {
                                        accessPermissions = 2;
                                    }
                                    return accessPermissions;
                                }
                            }
                        }
                    }
                }
            }
        }
        return 0;
    }

    public void setMDPPermission(PDDocument doc, PDSignature signature, int accessPermissions) {
        COSDictionary sigDict = signature.getCOSObject();

        COSDictionary transformParameters = new COSDictionary();
        transformParameters.setItem(COSName.TYPE, COSName.getPDFName("TransformParams"));
        transformParameters.setInt(COSName.P, accessPermissions);
        transformParameters.setName(COSName.V, "1.2");
        transformParameters.setNeedToBeUpdated(true);

        COSDictionary referenceDict = new COSDictionary();
        referenceDict.setItem(COSName.TYPE, COSName.getPDFName("SigRef"));
        referenceDict.setItem("TransformMethod", COSName.getPDFName("DocMDP"));
        referenceDict.setItem("DigestMethod", COSName.getPDFName("SHA1"));
        referenceDict.setItem("TransformParams", transformParameters);
        referenceDict.setNeedToBeUpdated(true);

        COSArray referenceArray = new COSArray();
        referenceArray.add(referenceDict);
        sigDict.setItem("Reference", referenceArray);
        referenceArray.setNeedToBeUpdated(true);

        COSDictionary catalogDict = doc.getDocumentCatalog().getCOSObject();
        COSDictionary permsDict = new COSDictionary();
        catalogDict.setItem(COSName.PERMS, permsDict);
        permsDict.setItem(COSName.DOCMDP, signature);
        catalogDict.setNeedToBeUpdated(true);
        permsDict.setNeedToBeUpdated(true);
    }

    private void verify(String dest) throws IOException {
        PDDocument document = PDDocument.load(new File(dest));
        List<PDSignature> signatures = document.getSignatureDictionaries();
        for (PDSignature signature : signatures) {
//            byte[] signatureContent = signature.
//            CMSSignedData cmsSignedData = new CMSSignedData(new CMSProcessableByteArray(signatureContent));
//            SignerInformation signerInformation = cmsSignedData.getSignerInfos().getSigners().iterator().next();
//            X509CertificateHolder certificateHolder = (X509CertificateHolder) cmsSignedData.getCertificates().getMatches(signerInformation.getSID()).iterator().next();
//            X509Certificate certificate = new JcaX509CertificateConverter().setProvider(new BouncyCastleProvider()).getCertificate(certificateHolder);
//            JcaSimpleSignerInfoVerifierBuilder verifierBuilder = new JcaSimpleSignerInfoVerifierBuilder().setProvider(new BouncyCastleProvider());
//            SignerInformationVerifier verifier = verifierBuilder.build(certificate);
//            try {
//                if (signerInformation.verify(verifier)) {
//                    System.out.println("The signature is valid.");
//                } else {
//                    System.out.println("The signature is not valid.");
//                }
//            } catch (CMSException e) {
//                throw new RuntimeException(e);
//            }
        }
        document.close();
    }
}
